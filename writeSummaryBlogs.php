<html>
	<head> 
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>
	<body>
		<?php
			$doc = new DOMDocument();
			$doc->load('http://www.gotoknow.org/blogs/posts?format=rss');
		
			$rootRss = $doc->documentElement;
			$listItems = $rootRss->getElementsByTagName("item"); 
			echo "Reading from summaryBlogs.xml...<br/>";
			
			$writer = new XMLWriter();
			$writer->openURI('summaryBlogs.xml');
			$writer->startDocument('1.0', 'UTF-8'); 
			$writer->startElement('items');
			
			foreach($listItems AS $item){			
				$title = $item->getElementsByTagname('title')->item(0)->nodeValue;
				$link = $item->getElementsByTagname('link')->item(0)->nodeValue;
				$author = $item->getElementsByTagname('author')->item(0)->nodeValue;
				
				echo $link , $author;
				
				$writer->startElement('item');
				$writer->writeElement('title',$title);
				$writer->writeElement('link',$link);
				$writer->writeElement('author',$author);
				$writer->endElement(); // endElement item
			}
			
			$writer->endElement(); // endElement items
			$writer->endDocument(); 
			$writer->flush();
						
		?>
	</body>
</html>