<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <title>SummaryBlogs</title>
				<h1>Recent Blog Entries at http://gotoknow.org</h1>
            </head>
            <body>
                <table border="1">
                    <tr  bgcolor="#9acd32">
                        <th align="left">Title</th><th align="left">Author</th><th align="left">Link</th>
                    </tr>
                    <xsl:for-each select="items/item">
                        <tr>
                            <td><xsl:value-of select="title"/></td>
                            <td><xsl:value-of select="author"/></td>
							<td><a>
								<xsl:attribute name="href">
									<xsl:value-of select="link"/>
								</xsl:attribute>
								<xsl:value-of select="link"/>
							</a></td>
                        </tr>                        
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
